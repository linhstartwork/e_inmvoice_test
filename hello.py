import lxml.html
from lxml import etree
 
xslt_doc = etree.parse("hello.xsl")
xslt_transformer = etree.XSLT(xslt_doc)
 
source_doc = etree.parse("hello.xml")
output_doc = xslt_transformer(source_doc)
 
output_doc.write("hello.html", pretty_print=True)